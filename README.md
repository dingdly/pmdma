# PMDMA
## Description
PM-DMA is a persistent memory file system with DMA. PM-DMA imports [I/OAT driver](https://elixir.bootlin.com/linux/v5.1/source/drivers/dma/ioat) into persistent memory file system. PM-DMA uses DMA devices to copy data between DRAM and PM when it reads and writes files. 

PM-DMA provides strong read and write performance:

 - Better concurrency performance.
 - Higher bandwidth when reading and writing big data at once.
 - Higher read and write throughput.

Currently, PM-DMA has been implemented in persistent memory file system: [NOVA](https://github.com/NVSL/NOVA) and [WineFS](https://github.com/utsaslab/winefs).
## content

1. `nova-dma/`contains PM-DMA source code based on NOVA.
2. `winefs-dma/` contains PM-DMA source code based on WineFS.

You can learn more about NOVA-PM-DMA and WineFS-PM-DMA by reading `READEME.md` in both directories.
## Building and Using
PM-DMA based on different file systems is built and used differently.For details of NOVA-PM-DMA construction and usage, click on link [NOVA-DMA Building and Using](https://gitlab.com/dingdly/pmdma/-/tree/master/nova-dma#building-and-using-nova-dma). For details of WineFS-PM-DMA construction and usage, click on link [WineFS-DMA Building and Using](https://gitlab.com/dingdly/pmdma/-/tree/master/winefs-dma#building-and-using-winefs-dma). 

## Current limitations

 - PM-DMA only works on x86-64 kernels.
 - PM-DMA only supports Intel processors with CBDMA

