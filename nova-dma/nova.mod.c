#include <linux/build-salt.h>
#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x2538353d, "module_layout" },
	{ 0x529dc6b9, "kobject_put" },
	{ 0x65c528e6, "kmem_cache_destroy" },
	{ 0xfb21f990, "dma_direct_unmap_sg" },
	{ 0x7e109259, "iget_failed" },
	{ 0xcbdf6b50, "kmalloc_caches" },
	{ 0xeef1035, "pci_write_config_dword" },
	{ 0x9e018a14, "pci_bus_type" },
	{ 0xeb233a45, "__kmalloc" },
	{ 0xf888ca21, "sg_init_table" },
	{ 0x234d814f, "drop_nlink" },
	{ 0x2183b8fc, "up_read" },
	{ 0xa5ffe3a7, "alloc_dca_provider" },
	{ 0xf11928e1, "make_bad_inode" },
	{ 0x8b3b727e, "generic_file_llseek" },
	{ 0x37607cab, "__put_devmap_managed_page" },
	{ 0x1fcbef72, "__mark_inode_dirty" },
	{ 0xd6ee688f, "vmalloc" },
	{ 0xba738cd8, "single_open" },
	{ 0xf31acc74, "generic_write_checks" },
	{ 0xc39aa6c9, "param_ops_int" },
	{ 0x754d539c, "strlen" },
	{ 0x60a13e90, "rcu_barrier" },
	{ 0xaf076aec, "nd_fletcher64" },
	{ 0x815b5dd4, "match_octal" },
	{ 0xad73041f, "autoremove_wake_function" },
	{ 0x3d02a9f8, "generic_fh_to_parent" },
	{ 0x1db7706b, "__copy_user_nocache" },
	{ 0x79aa04a2, "get_random_bytes" },
	{ 0x51106620, "single_release" },
	{ 0xf335ce9d, "seq_puts" },
	{ 0x84f15016, "boot_cpu_data" },
	{ 0xeedb9303, "is_bad_inode" },
	{ 0xb661c6e0, "generic_file_open" },
	{ 0xb3635b01, "_raw_spin_lock_bh" },
	{ 0xfff5afc, "time64_to_tm" },
	{ 0x3ce9a278, "touch_atime" },
	{ 0xc0a3d105, "find_next_bit" },
	{ 0xdf566a59, "__x86_indirect_thunk_r9" },
	{ 0x5055c493, "seq_printf" },
	{ 0xb43f9365, "ktime_get" },
	{ 0x8ac2e402, "remove_proc_entry" },
	{ 0x6729d3df, "__get_user_4" },
	{ 0x44e9a829, "match_token" },
	{ 0xc29957c3, "__x86_indirect_thunk_rcx" },
	{ 0x1623fa4a, "inc_nlink" },
	{ 0x87b8798d, "sg_next" },
	{ 0xa21f114d, "dma_async_tx_descriptor_init" },
	{ 0xfe2955e5, "init_user_ns" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0xa6093a32, "mutex_unlock" },
	{ 0x19a37629, "mount_bdev" },
	{ 0x85df9b6c, "strsep" },
	{ 0x5e07f72d, "kobject_del" },
	{ 0x6672e75b, "generic_read_dir" },
	{ 0x999e8297, "vfree" },
	{ 0x174fbafc, "dma_free_attrs" },
	{ 0x28aa6a67, "call_rcu" },
	{ 0x7a2af7b4, "cpu_number" },
	{ 0x97651e6c, "vmemmap_base" },
	{ 0xb15b4109, "crc32c" },
	{ 0x3c3ff9fd, "sprintf" },
	{ 0x953e1b9e, "ktime_get_real_seconds" },
	{ 0x26948d96, "copy_user_enhanced_fast_string" },
	{ 0x6ee9ae6f, "seq_read" },
	{ 0xc7a17a48, "pv_ops" },
	{ 0xffc0023c, "kthread_create_on_node" },
	{ 0x15ba50a6, "jiffies" },
	{ 0x2a1766d7, "down_read" },
	{ 0xd41f5402, "cpumask_next" },
	{ 0xd29c2ec3, "param_ops_string" },
	{ 0xe6a7dabe, "kthread_bind" },
	{ 0xece784c2, "rb_first" },
	{ 0xde9e20c8, "make_kgid" },
	{ 0xd9a5ea54, "__init_waitqueue_head" },
	{ 0xb44ad4b3, "_copy_to_user" },
	{ 0xb91afcc5, "PDE_DATA" },
	{ 0x17de3d5, "nr_cpu_ids" },
	{ 0x57adc999, "dmaengine_unmap_put" },
	{ 0x5656da2a, "inode_owner_or_capable" },
	{ 0x97934ecf, "del_timer_sync" },
	{ 0x7e526bfa, "__x86_indirect_thunk_r10" },
	{ 0x30a1c82e, "_dev_warn" },
	{ 0xfb578fc5, "memset" },
	{ 0x2548c032, "__cpu_possible_mask" },
	{ 0x65a51c70, "from_kuid" },
	{ 0x77ec6010, "proc_mkdir" },
	{ 0xf5e60777, "current_task" },
	{ 0xa50bcff0, "x86_cpu_to_apicid" },
	{ 0x9a76f11f, "__mutex_init" },
	{ 0xc5850110, "printk" },
	{ 0xbcab6ee6, "sscanf" },
	{ 0x4d8bba66, "kthread_stop" },
	{ 0xce6775a, "d_obtain_alias" },
	{ 0x449ad0a7, "memcmp" },
	{ 0x564405cb, "__cpu_online_mask" },
	{ 0xdadb03c, "dax_writeback_mapping_range" },
	{ 0x265d17fc, "kobject_init_and_add" },
	{ 0x1edb69d6, "ktime_get_raw_ts64" },
	{ 0x5e71d44b, "timespec64_trunc" },
	{ 0xafb8c6ff, "copy_user_generic_string" },
	{ 0xf1e046cc, "panic" },
	{ 0x4c9d28b0, "phys_base" },
	{ 0x99292d68, "file_remove_privs" },
	{ 0x479c3c86, "find_next_zero_bit" },
	{ 0xfaef0ed, "__tasklet_schedule" },
	{ 0xa1c76e0a, "_cond_resched" },
	{ 0x4d9b652b, "rb_erase" },
	{ 0xf3341268, "__clear_user" },
	{ 0x905d7ea9, "from_kgid" },
	{ 0x5a921311, "strncmp" },
	{ 0x4de57e8a, "pci_read_config_word" },
	{ 0xbd35662f, "dma_direct_map_page" },
	{ 0xfb481954, "vprintk" },
	{ 0x2e93391f, "dma_alloc_attrs" },
	{ 0x4b1a15cf, "kmem_cache_free" },
	{ 0x41aed6e7, "mutex_lock" },
	{ 0xd4981f90, "set_nlink" },
	{ 0x6fc7bab, "file_update_time" },
	{ 0x4a8c641f, "setattr_copy" },
	{ 0xc38c83b8, "mod_timer" },
	{ 0x6be14f62, "insert_inode_locked" },
	{ 0x2f7754a8, "dma_pool_free" },
	{ 0xca95261f, "truncate_pagecache" },
	{ 0x6d9d5e65, "thp_get_unmapped_area" },
	{ 0x4e3567f7, "match_int" },
	{ 0x19f1ffb3, "up_write" },
	{ 0xe60715a0, "_dev_err" },
	{ 0xb546922d, "down_write" },
	{ 0x54f62566, "pci_enable_msi" },
	{ 0x952664c5, "do_exit" },
	{ 0xfe487975, "init_wait_entry" },
	{ 0x4e6e4b41, "radix_tree_delete" },
	{ 0xfe5d4bb2, "sys_tz" },
	{ 0xe523ad75, "synchronize_irq" },
	{ 0xeef9510d, "find_get_entries_tag" },
	{ 0x82072614, "tasklet_kill" },
	{ 0x5ea430d4, "inode_init_once" },
	{ 0x72a98fdb, "copy_user_generic_unrolled" },
	{ 0x7cd8d75e, "page_offset_base" },
	{ 0x3423955, "mnt_drop_write_file" },
	{ 0xc6cbbc89, "capable" },
	{ 0x11b96301, "dma_direct_unmap_page" },
	{ 0x6958ae23, "dax_get_by_host" },
	{ 0x76d451c4, "add_taint" },
	{ 0x40a9b349, "vzalloc" },
	{ 0x6b27729b, "radix_tree_gang_lookup" },
	{ 0x82bfdb19, "kmem_cache_alloc" },
	{ 0xb601be4c, "__x86_indirect_thunk_rdx" },
	{ 0x69049cd2, "radix_tree_replace_slot" },
	{ 0xb2fd5ceb, "__put_user_4" },
	{ 0x4e20bcf8, "radix_tree_tag_set" },
	{ 0x10f9335, "pci_enable_msix_range" },
	{ 0xb84e122d, "make_kuid" },
	{ 0x41efdeaf, "radix_tree_lookup_slot" },
	{ 0x79717e39, "devm_free_irq" },
	{ 0xf82ec573, "rb_prev" },
	{ 0xfa37bebf, "register_dca_provider" },
	{ 0x49c41a57, "_raw_spin_unlock_bh" },
	{ 0xdecd0b29, "__stack_chk_fail" },
	{ 0x8ddd8aad, "schedule_timeout" },
	{ 0x1000e51, "schedule" },
	{ 0x1d24c881, "___ratelimit" },
	{ 0x47941711, "_raw_spin_lock_irq" },
	{ 0x7865dc4b, "unlock_new_inode" },
	{ 0xcd8951a7, "mnt_want_write_file" },
	{ 0xca09a229, "kill_block_super" },
	{ 0x6b2dc060, "dump_stack" },
	{ 0xd7f4aff2, "inode_newsize_ok" },
	{ 0x2ea2c95c, "__x86_indirect_thunk_rax" },
	{ 0x64a1894f, "pci_read_config_dword" },
	{ 0x21547afb, "dev_driver_string" },
	{ 0x3efd1889, "dax_direct_access" },
	{ 0x678b96ec, "dma_pool_alloc" },
	{ 0xa507b827, "wake_up_process" },
	{ 0x99ce77fc, "get_user_pages_remote" },
	{ 0xbdfb6dbb, "__fentry__" },
	{ 0x4801057d, "kmem_cache_alloc_trace" },
	{ 0xdbf17652, "_raw_spin_lock" },
	{ 0x19cbdca9, "__dynamic_dev_dbg" },
	{ 0x5ecfeec6, "__per_cpu_offset" },
	{ 0x1c7d65ec, "__memcpy_mcsafe" },
	{ 0xa5526619, "rb_insert_color" },
	{ 0x3b015ea5, "change_protection" },
	{ 0x501d9443, "kmem_cache_create" },
	{ 0xd136c091, "register_filesystem" },
	{ 0x3eeb2322, "__wake_up" },
	{ 0xb444fd2f, "dax_iomap_fault" },
	{ 0xb3f7646e, "kthread_should_stop" },
	{ 0x8c26d495, "prepare_to_wait_event" },
	{ 0xd8f14087, "proc_create_data" },
	{ 0x865a89e, "free_dca_provider" },
	{ 0x91435f4d, "seq_lseek" },
	{ 0x189f4c1e, "iput" },
	{ 0x194abd20, "dax_iomap_rw" },
	{ 0x9e7d436a, "generic_file_fsync" },
	{ 0x37a0cba, "kfree" },
	{ 0x1235a772, "inode_dio_wait" },
	{ 0xbcef4780, "dma_direct_map_sg" },
	{ 0x46d22b41, "remap_pfn_range" },
	{ 0xb5cbb2af, "ihold" },
	{ 0x69acdf38, "memcpy" },
	{ 0x794518ec, "__sb_end_write" },
	{ 0x99c011d8, "mcsafe_key" },
	{ 0x33e3e273, "current_time" },
	{ 0xd5fd90f1, "prepare_to_wait" },
	{ 0x166028d1, "pci_disable_msi" },
	{ 0x85518570, "d_splice_alias" },
	{ 0x78f49135, "__sb_start_write" },
	{ 0x9c70ac10, "d_make_root" },
	{ 0xb352177e, "find_first_bit" },
	{ 0x529a5de5, "pci_get_device" },
	{ 0x92540fbf, "finish_wait" },
	{ 0x70ad75fb, "radix_tree_lookup" },
	{ 0x63c4d61f, "__bitmap_weight" },
	{ 0xca9360b5, "rb_next" },
	{ 0xfaecedfc, "unregister_filesystem" },
	{ 0xcc40949e, "init_special_inode" },
	{ 0xb1cd75e4, "pci_dev_put" },
	{ 0x494e3393, "vm_get_page_prot" },
	{ 0x29361773, "complete" },
	{ 0x943ab2f0, "new_inode" },
	{ 0x864ffc4a, "__bdev_dax_supported" },
	{ 0x9c0c83b3, "noop_fsync" },
	{ 0x4d1ff60a, "wait_for_completion_timeout" },
	{ 0x9d64d1c1, "generic_fh_to_dentry" },
	{ 0x362ef408, "_copy_from_user" },
	{ 0x6fbc6a00, "radix_tree_insert" },
	{ 0x5182fc27, "devm_request_threaded_irq" },
	{ 0xee450c2a, "clear_inode" },
	{ 0x4339f57c, "d_instantiate" },
	{ 0xf7fb7276, "dma_ops" },
	{ 0x52b02799, "__put_page" },
	{ 0xc464bb73, "clear_nlink" },
	{ 0xf952a32, "iget_locked" },
	{ 0x655d2ab0, "vfs_fsync_range" },
	{ 0xeab9aa7e, "setattr_prepare" },
	{ 0xf308e6c0, "generic_fillattr" },
	{ 0x321c89f5, "inode_init_owner" },
	{ 0x484f6edf, "ktime_get_coarse_real_ts64" },
	{ 0x9f5d77db, "truncate_inode_pages" },
	{ 0x587f22d7, "devmap_managed_key" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=dca,libcrc32c";

MODULE_ALIAS("pci:v00008086d00003430sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003431sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003432sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003433sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003429sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000342Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000342Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d0000342Csv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003710sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003711sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003712sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003713sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003714sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003715sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003716sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003717sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003718sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003719sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003C20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003C21sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003C22sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003C23sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003C24sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003C25sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003C26sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003C27sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003C2Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00003C2Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000E20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000E21sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000E22sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000E23sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000E24sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000E25sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000E26sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000E27sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000E2Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000E2Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002F20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002F21sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002F22sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002F23sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002F24sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002F25sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002F26sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002F27sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002F2Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002F2Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F20sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F21sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F22sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F23sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F24sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F25sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F26sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F27sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F2Esv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F2Fsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00002021sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000C50sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000C51sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000C52sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00000C53sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F50sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F51sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F52sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008086d00006F53sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "B05BE88D239DA4D58B38500");
