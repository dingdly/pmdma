# NOVA-DMA
## Description

[NOVA](https://github.com/NVSL/NOVA) is primarily a log-structured file system.NOVA's goal is to provide a high-performance, full-featured, production-ready file system tailored for byte-addressable non-volatile memories (e.g., NVDIMMs and Intel's soon-to-be-released 3DXpoint DIMMs). It combines design elements from many other file systems to provide a combination of high-performance, strong consistency guarantees, and comprehensive data protection. NOVA was developed by the [Non-Volatile Systems Laboratory](http://nvsl.ucsd.edu/ "http://nvsl.ucsd.edu")in the [Computer Science and Engineering Department](http://cs.ucsd.edu/) at the [University of California, San Diego](http://www.ucsd.edu/).

[NOVA-DMA](https://gitlab.com/dingdly/pmdma/-/tree/master/nova-dma) is a file system implemented by modifying NOVA (5.1.0) source code.NOVA-DMA imports [I/OAT driver source code](https://elixir.bootlin.com/linux/v5.1/source/drivers/dma/ioat) into NOVA, so that NOVA can use DMA engine instead of CPU to perform the copying work between DRAM and PM.

## Contents
The files `init.c`,`init.h` `dax.c`,`dma.c`,`dma.h` `hw.h`,`sysfs.c` ,`prep.c`,`registers.h`and `sysfs.c` are transplanted from IOAT driver source, and the remaining files are the NOVA source code.

We have made changes to the following files:

1. `init.c`   contains the interface for memory copying.
2. `file.c`   the interface "nova_dax_file_read" is modified, and the interface "do_dax_mapping_read_ioat"  is added in the source code.
3. `dax.c`    the interface "nova_inplace_file_write"  is modified, and the interface "do_nova_inplace_file_write_ioat" is added in the source code.
4. `nova.h`  contains  interface reference to the file init.c.
5. `super.h` add the struct for I/OAT devices.
6. `super.c` the interfaces "nova_put_super" and "nova_init" are modified.
## System Requirements
1. Intel processor with CBDMA（Crystal Beach DMA）
2. Linux kernel 5.1.0
3. Ubuntu 16.04 / 18.04
## Building and Using NOVA-DMA
NOVA-DMA is builded and used by the same commands as NOVA.
### Building NOVA-DMA

NOVA-DMA works on the 5.1.0 version of x86-64 Linux kernel.

To build NOVA, simply run a
```
#make
```
command.
### Running NOVA-DMA
The /dev/pmem0 is a persistent memory device running in App Direct mode.
```
# insmod nova.ko
# mount -t NOVA -o init /dev/pmem0 /mnt/ramdisk 
```
After the file system is mounted, when you read or write to a file on NOVA-DMA, the DMA engine will be called to complete the copying work between DRAM and PM.
