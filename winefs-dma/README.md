# WineFS-DMA
## Description

[WineFS](https://github.com/utsaslab/WineFS) is a file system for Persistent Memory (PM) which is aimed at maximizing the performance of memory-mapped applications. WineFS uses a novel dual-allocation policy: the memory-mapped application files are allocated with the help of a huge-page aligned allocator whereas the other POSIX application files are allocated with a hole-filling allocator. With the help of dual allocators, WineFS is able to preserve the huge-page-aligned free extents for the memory-mapped applications, in the presence of aging and at high utilization. WineFS uses per-CPU journaling for crash consistency, thus providing high performance and scalability for metadata-heavy workloads as well.

[WineFS-DMA](https://gitlab.com/dingdly/pmdma/-/tree/master/winefs-dma)  is a file system implemented by modifying WineFS (5.1.0) source code.WineFS-DMA imports [I/OAT driver source code](https://elixir.bootlin.com/linux/v5.1/source/drivers/dma/ioat) into WineFS, so that WineFS can use DMA engine instead of CPU to perform the copying work between DRAM and PM.

## Contents
The files `init.c`,`init.h` `dax.c`,`dma.c`,`dma.h` `hw.h`,`sysfs.c` ,`prep.c`,`registers.h`and `sysfs.c` are transplanted from IOAT driver source, and the remaining files are the WineFS source code.

We have made changes to the following files:

1. `init.c`   contains the interface for memory copying.
2. `xip.c`   the interface "do_xip_mapping_read" and "memcpy_to_nvmm" are modified, and the interface "do_xip_mapping_read_ioat" and "memcpy_to_nvmm_ioat" are added in the source code.
4. `pmfs.h`  contains  interface reference to the file init.c.
5. `super.h` add the struct for I/OAT devices.
6. `super.c` the interfaces "pmfs_put_super" and "pmfs_init" are modified.
## System Requirements
1. Intel processor with CBDMA（Crystal Beach DMA）
2. Linux kernel 5.1.0
3. Ubuntu 16.04 / 18.04
## Building and Using WineFS-DMA
WineFS-DMA is builded and used by the same commands as WineFS.
### Building WineFS-DMA

WineFS-DMA works on the 5.1.0 version of x86-64 Linux kernel.

To build WineFS, simply run a
```
#make
```
command.
### Running WineFS-DMA
The /dev/pmem0 is a persistent memory device running in App Direct mode.
```
# insmod winefs.ko
# mount -t WineFS -o init /dev/pmem0 /mnt/ramdisk 
```
After the file system is mounted, when you read or write to a file on WineFS-DMA, the DMA engine will be called to complete the copying work between DRAM and PM.
